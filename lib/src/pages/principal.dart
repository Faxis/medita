import 'package:flutter/material.dart';
import 'package:medita/src/pages/blog.dart';
import 'package:medita/src/pages/contenido_principal.dart';

import 'package:medita/src/pages/cronometro.dart';

class Principal extends StatefulWidget {
  @override
  _PrincipalState createState() => _PrincipalState();
}

class _PrincipalState extends State<Principal> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  int _selectedIndex = 0;
  String _tituloSection = 'Medita';
  Color _colorAppbar = Color.fromRGBO(30, 80, 120, 1);
  BuildContext context;
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  List<Widget> _displayOptions = <Widget>[
    ContenidoPrincipal(),
    Blog(),
    Cronometro()
  ];

  @override
  Widget build(BuildContext context) {
    this.context = context;
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
  
        title: Text('$_tituloSection'),
        backgroundColor: _colorAppbar,
       actions: <Widget>[
          IconButton(
              icon: Icon(Icons.contact_phone),
              onPressed: () {
                Navigator.pushNamed(context, 'contacto');
              }),
        ],
        leading: IconButton(
            icon: Icon(Icons.menu),
            onPressed: () {
              _scaffoldKey.currentState.openDrawer();
            }),
      ),
      body: _displayOptions.elementAt(_selectedIndex),
      bottomNavigationBar: barraNavigator(),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.all(0),
          children: <Widget>[
            UserAccountsDrawerHeader(
              otherAccountsPictures: <Widget>[],
              currentAccountPicture: CircleAvatar(
                backgroundColor: Colors.white,
                child: Icon(
                  Icons.people,
                  size: 30,
                  color: Colors.lightBlueAccent,
                ),
              ),
              accountName: Text(
                'Invitado',
                style: TextStyle(fontSize: 25),
              ),
              accountEmail: Text('ejemplo@gmail.com'),
              decoration: BoxDecoration(
                color: Colors.lightBlueAccent,
              ),
            ),
            ListTile(
              title: Text('Salir'),
              onTap: () {},
              trailing: Icon(Icons.exit_to_app),
            )
          ],
        ),
      ),
    );
  }

  BottomNavigationBar barraNavigator() {
    return BottomNavigationBar(
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.home),
          label: 'Inicio',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.dashboard),
          label: 'Blog',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.access_time),
          label: 'Cronometro',
        ),
      ],
      currentIndex: _selectedIndex,
      selectedItemColor: Colors.white,
      backgroundColor: Color.fromRGBO(144, 150, 249, 1),
      onTap: onTap,
      elevation: 0,
    );
  }

  void onTap(int value) {
    setState(() {
      _selectedIndex = value;
      if (_selectedIndex == 0) {
        _tituloSection = 'Medita';
        _colorAppbar = Color.fromRGBO(30, 80, 120, 1);
      } else if (_selectedIndex == 1) {
        _tituloSection = 'Blog';
        _colorAppbar = Colors.blue;
      } else if (_selectedIndex == 2) {
        _tituloSection = 'Cronometro';
        _colorAppbar = Color.fromRGBO(120, 100, 250, 1);
      }
    });
  }
}
