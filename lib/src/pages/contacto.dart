import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class Contacto extends StatelessWidget {
  
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final snackBar = SnackBar(

    content: Text('Copiado en el portapapeles con exito'),
    elevation: 0.0,
    duration: Duration(seconds: 3),
    action: SnackBarAction(
      label: 'Cerrar',
      onPressed: () {
        // Some code to undo the change.
      },
    ),
  );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Contactenos'),
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: <Color>[Colors.cyan, Colors.purple])),
        ),
        bottom: PreferredSize(
          preferredSize: const Size.fromHeight(110.0),
          child: Theme(
            data: Theme.of(context).copyWith(accentColor: Colors.white),
            child: Container(
              //height: 48.0,
              alignment: Alignment.center,
              child: Image(
                  image: AssetImage('assets/images/contacto.png'),
                  width: (MediaQuery.of(context).size.width / 3)),
            ),
          ),
        ),
      ),
      body: ListView(
        shrinkWrap: true,
        children: <Widget>[
          cardInfo(
            colors: [Colors.redAccent, Colors.orange],
            title: 'Correo Electronico:',
            subtitle: [Text('ejemplo@systemkw.com')],
            icon: Icon(Icons.alternate_email),
            copy: Icon(Icons.content_copy),
            action: () {
              Clipboard.setData(ClipboardData(text: 'ejemplo@systemkw.com'));
              _scaffoldKey.currentState.showSnackBar(snackBar);
            },
          ),
          cardInfo(
              colors: [Colors.orangeAccent, Colors.yellowAccent],
              title: 'WhatsApp:',
              subtitle: [Text('5544332211')],
              icon: Icon(Icons.phone_in_talk),
              copy: Icon(Icons.content_copy),
              action: () {
                Clipboard.setData(ClipboardData(text: '5544332211'));

                // Find the Scaffold in the widget tree and use
                // it to show a SnackBar.
                _scaffoldKey.currentState.showSnackBar(snackBar);
              }),
          cardInfo(
              colors: [Colors.yellow, Colors.green],
              title: 'Redes Sociales:',
              subtitle: <Widget>[
                Text('Facebook: ····'),
                Text('Twitter: ···'),
                Text('Instagram: ···')
              ],
              icon: Icon(Icons.group_work)),
          cardInfo(
              colors: [Colors.cyan, Colors.blue],
              title: 'Desarrolladores:',
              subtitle: <Widget>[
                Text('SystemKW: Todos los derechos reservados'),
              ],
              icon: Icon(Icons.developer_mode)),
        ],
      ),
    );
  }

  Widget cardInfo(
      {List<Color> colors,
      String title,
      List<Widget> subtitle,
      Icon icon,
      Icon copy,
      void action()}) {
    return Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: colors),
            borderRadius: BorderRadius.circular(6),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(.3),
                spreadRadius: 0,
                blurRadius: 2,
                offset: Offset(0, 1), // changes position of shadow
              ),
            ]),
        padding: EdgeInsets.all(10),
        margin: EdgeInsets.all(10),
        child: ListTile(
            onTap: action,
            trailing: copy,
            leading: icon,
            title: Text(title),
            subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: subtitle,
            )));
  }
}
