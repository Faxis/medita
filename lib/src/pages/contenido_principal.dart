import 'package:flutter/material.dart';

class ContenidoPrincipal extends StatefulWidget {
  @override
  _ContenidoPrincipalState createState() => _ContenidoPrincipalState();
}

class _ContenidoPrincipalState extends State<ContenidoPrincipal> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: contenidoPrincipal(),
    );
  }

  Widget contenidoPrincipal() {
    return ListView(
      shrinkWrap: true,
      children: <Widget>[
        imagenRepresentativa(),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            cajaMusica(
                title: 'Música',
                image: 'musica.jpg',
                onTap: () {
                  Navigator.pushNamed(context, 'musica');
                }),
            cajaMusica(
                title: 'Tecnicas',
                image: 'tecnicas.jpg',
                onTap: () {
                  Navigator.pushNamed(context, 'tecnicas');
                }),
            cajaMusica(
                title: 'Mantra',
                image: 'mantra.jpg',
                onTap: () {
                  print('cc');
                }),
          ],
        ),
        cardInfo(
            title: '¿Que es la meditacíon?',
            subtitle:
                'La meditación es un entrenamiento para la mente y el corazón que lleva una mayor libertad mental y emocional.',
            color: Colors.blue[300]),
        cardInfo(
          title: '¿Por qué es importante meditar?',
          subtitle:
              'Los principales beneficios que brinda la meditación es la posibilidad de conocerse uno mismo, o mejor dicho conocerse más profundamente, acceder a sensaciones, tanto físicas como emocionales, que creen un vínculo mucho más fuerte con el yo interior.',
          //icon: Icon(Icons.question_answer),
          color: Colors.grey[100],
        )
      ],
    );
  }

  static Widget cajaMusica({String title, String image, void onTap()}) {
    return LayoutBuilder(
      builder: (context, constraints) {
        double wh = (MediaQuery.of(context).size.width - 25) / 3;
        return Container(
          constraints: BoxConstraints.expand(height: wh, width: wh),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5.0),
            image: DecorationImage(
                image: AssetImage(
                  'assets/images/$image',
                ),
                fit: BoxFit.cover),
          ),
          child: FlatButton(
              onPressed: onTap,
              child: Center(
                child: Text(
                  title,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontFamily: 'GreatVibes',
                      fontSize: 24,
                      color: Colors.white),
                ),
              )),
        );
      },
    );
  }

  static Widget imagenRepresentativa() {
    return Container(
        height: 220.0,
        padding: EdgeInsets.all(10),
        margin: EdgeInsets.only(bottom: 5),
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(
                'assets/images/principal.jpg',
              ),
              fit: BoxFit.cover),
        ),
        child: Center(
          child: Text(
            'La magia de la meditación curiosamente sucede, no cuándo estamos recluidos en silencio, sino cuándo regresamos a interactuar con el mundo.',
            style: TextStyle(
                fontFamily: 'GreatVibes',
                fontSize: 24.0,
                color: Colors.white,
                textBaseline: TextBaseline.ideographic,
                height: 1.5),
            textAlign: TextAlign.center,
          ),
        ));
  }

  static Widget cardInfo({String title, String subtitle, Color color}) {
    return Card(
      elevation: 0,
      color: color,
      child: Container(
        padding: EdgeInsets.all(4),
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(5),
              child: Text(
                title,
                style: TextStyle(
                    color: Colors.black45,
                    fontFamily: 'GreatVibes',
                    fontSize: 24),
              ),
            ),
            Container(
              padding: EdgeInsets.all(10),
              child: Text(
                subtitle,
                textAlign: TextAlign.justify,
                style: TextStyle(height: 1.5),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
