import 'package:flutter/material.dart';

class Blog extends StatefulWidget {
  Blog({Key key}) : super(key: key);

  @override
  _BlogState createState() => _BlogState();
}

class _BlogState extends State<Blog> {
  @override
  void dispose() {
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        topImage(),
        cajaInfo(
            title: 'Top 10 de Tecnicas',
            subtitle: 'toma nota esto te puede ayudar..',
            icon: Icon(Icons.star),
            elemento: () {},
            view: '312'),
        cajaInfo(
            title: 'Consejos basicos',
            subtitle: 'Cosas basicas que te volveran experto..',
            icon: Icon(Icons.dashboard),
            elemento: () {},
            view: '40'),
        cajaInfo(
            title: 'Importancia de meditar',
            subtitle: 'las razones del porque..',
            icon: Icon(Icons.dashboard),
            elemento: () {},
            view: '100'),
      ],
    );
  }

  Container cajaInfo(
      {String title,
      String subtitle,
      Icon icon,
      String view,
      void elemento()}) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.grey[50],
        borderRadius: BorderRadius.all(Radius.circular(2)),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.3),
            spreadRadius: 1,
            blurRadius: 2,
            offset: Offset(0, 1), // changes position of shadow
          ),
        ],
      ),
      margin: EdgeInsets.all(10),
      child: ListTile(
        leading: icon,
        trailing: Container(
          padding: EdgeInsets.all(6),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(6), color: Colors.blue[200]),
          child: Column(
            children: <Widget>[
              Icon(Icons.remove_red_eye),
              Text(
                view,
                style: TextStyle(color: Colors.black87),
              )
            ],
          ),
        ),
        title: Text(title),
        subtitle: Container(
          padding: EdgeInsets.only(top: 5, bottom: 3, left: 2, right: 2),
          child: Column(
            children: <Widget>[
              Text(
                subtitle,
                textAlign: TextAlign.start,
                style: TextStyle(fontSize: 11.0),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  FlatButton(
                      color: Colors.deepOrange[300],
                      onPressed: elemento,
                      child: Text('Ver Mas')),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  Container topImage() {
    return Container(
      padding: EdgeInsets.all(10),
      child: ClipRRect(
        borderRadius: BorderRadius.all(Radius.circular(10)),
        child: FadeInImage.assetNetwork(
            //fit: BoxFit.fill,
            placeholderScale: 3.0,
            placeholder: 'assets/images/spinner.gif',
            image: 'https://cdn.hipwallpaper.com/i/51/4/zm9cU2.jpg'),
      ),
    );
  }
}
