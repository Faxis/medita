import 'package:flutter/material.dart';
import 'package:medita/src/controller/videos_controller.dart';

import 'package:youtube_api_v3/youtube_api_v3.dart';

const apiKey = 'AIzaSyA8clqRyAEPTNP4EtiLZYg-yY_HlX-mRBM';

class Tecnicas extends StatefulWidget {
  @override
  _TecnicasState createState() => _TecnicasState();
}

class _TecnicasState extends State<Tecnicas> {
  List<PlayListItem> videos = new List();
  PlayListItemListResponse paginaActual;
  List<String> imagenes = new List();

  Map<String, String> listasR = {
    'Iniciación': 'PL2rlMIrfyUv8fPPJxZC9vSHRDaTrNJs1_',
    'Clase de pilates': 'PL2rlMIrfyUv86kPZlmbFCsPYlJo9w2aSR',
  };

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {  
    super.initState();

    listasR.forEach((key, value) async {
      await obtenerVideos(value);
      //sleep(const Duration(seconds: 1));
    });
  }
  
  setVideos(videos) {
    setState(() {
      this.videos = videos;
    });
  }

/*   Navigator.of(context).push(MaterialPageRoute(builder: (context) => UserData("WonderClientName",132)));
*/

  Future obtenerVideos(String idPlist) async {
    YoutubeAPIv3 api = new YoutubeAPIv3(apiKey);
    PlayListItemListResponse playlist = await api.playListItems(
        playlistId: idPlist, maxResults: 1, part: Parts.snippet);
    var x;
    var videos = playlist.items.map((video) {
      x = video.snippet.thumbnails.maxres.url;
      return video;
    }).toList();
    imagenes.add(x);
    paginaActual = playlist;
    this.videos.addAll(videos);
    setVideos(this.videos);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Tecnicas'),
        backgroundColor: Color.fromRGBO(100, 130, 222, 1.0),
        excludeHeaderSemantics: true,
      ),
      body: listaCargada(),
    );
  }

  Widget listaCargada() {
    return Center(
        child: ListView.builder(
      padding: EdgeInsets.only(top: 10),
      itemCount: videos.length,
      itemBuilder: (BuildContext context, int index) {
        double wh = (MediaQuery.of(context).size.width - 25) / 3;
        return ListTile(
          title: Container(
            constraints: BoxConstraints.expand(height: wh, width: wh),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5.0),
              image: DecorationImage(
                  image: NetworkImage(imagenes[index]), fit: BoxFit.cover),
            ),
            child: FlatButton(
                color: Colors.black26,
                highlightColor: Color.fromRGBO(100, 200, 250, 0.7),
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => VideosController(
                            title: listasR.keys.elementAt(index),
                            idList: listasR.values.elementAt(index),
                          )));
                },
                child: Center(
                  child: Text(
                    listasR.keys.elementAt(index),
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontFamily: 'GreatVibes',
                        fontSize: 24,
                        shadows: [Shadow(blurRadius: 1, color: Colors.black)],
                        color: Colors.white),
                  ),
                )),
          ),
        );
      },
    ));
  }
}

class Video {
  final String thumbnail;
  Video(this.thumbnail);
}
