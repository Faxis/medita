import 'package:flutter/material.dart';

class Musica extends StatefulWidget {
  Musica({Key key}) : super(key: key);

  @override
  _MusicaState createState() => _MusicaState();
}

class _MusicaState extends State<Musica> {
  double _value = 0.0;
  Icon pausaValor = Icon(Icons.play_arrow);
  bool isPausaValor = true;
  Color colorPausaValor = Colors.red[400];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Musica'),
        backgroundColor: Colors.teal[400],
      ),
      body: Container(
        color: Colors.grey[100],
        padding: EdgeInsets.all(10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            imagenPrincipalRadio(context),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  FlatButton(
                      shape: _CustomBorder(),
                      padding: EdgeInsets.all(30),
                      onPressed: () {},
                      child: Icon(Icons.arrow_back_ios)),
                  FlatButton(
                      shape: _CustomBorder(),
                      padding: EdgeInsets.all(30),
                      color: colorPausaValor,
                      onPressed: () {
                        setState(() {
                          if (isPausaValor) {
                            pausaValor = Icon(Icons.pause);
                            colorPausaValor = Colors.indigoAccent[200];
                          } else {
                            pausaValor = Icon(Icons.play_arrow);
                            colorPausaValor = Colors.red[400];
                          }
                          isPausaValor = !isPausaValor;
                        });
                      },
                      child: pausaValor),
                  FlatButton(
                      shape: _CustomBorder(),
                      padding: EdgeInsets.all(30),
                      onPressed: () {},
                      child: Icon(Icons.arrow_forward_ios)),
                ],
              ),
            ),
            volumenContainer(context),
          ],
        ),
      ),
    );
  }

  Container volumenContainer(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(3)),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              spreadRadius: 0.4,
              blurRadius: 1,
              offset: Offset(0, 1), // changes position of shadow
            ),
          ],
        ),
        width: MediaQuery.of(context).size.width * 2,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                color: Colors.blue[400],
                borderRadius: BorderRadius.all(Radius.circular(10)),
              ),
              padding: EdgeInsets.all(6),
              child: Row(
                children: <Widget>[
                  Icon(
                    Icons.volume_up,
                    color: Colors.black45,
                  ),
                  Text(
                    'Volumen',
                    style: TextStyle(
                      color: Colors.black45,
                    ),
                  )
                ],
              ),
            ),
            sliderVolumen(context),
          ],
        ));
  }

  SliderTheme sliderVolumen(BuildContext context) {
    return SliderTheme(
      data: SliderTheme.of(context).copyWith(
        activeTrackColor: Colors.blue[700],
        inactiveTrackColor: Colors.blue[100],
        trackShape: RoundedRectSliderTrackShape(),
        trackHeight: 4.0,
        thumbShape: RoundSliderThumbShape(enabledThumbRadius: 12.0),
        thumbColor: Colors.blueAccent,
        overlayColor: Colors.cyan.withAlpha(32),
        overlayShape: RoundSliderOverlayShape(overlayRadius: 28.0),
        tickMarkShape: RoundSliderTickMarkShape(),
        activeTickMarkColor: Colors.blue[700],
        inactiveTickMarkColor: Colors.blue[100],
        valueIndicatorShape: PaddleSliderValueIndicatorShape(),
        valueIndicatorColor: Colors.blueAccent,
        valueIndicatorTextStyle: TextStyle(
          color: Colors.white,
        ),
      ),
      child: Slider(
        value: _value,
        min: 0,
        max: 100,
        divisions: 10,
        label: '$_value',
        onChanged: (value) {
          setState(
            () {
              _value = value;
            },
          );
        },
      ),
    );
  }

  Image imagenPrincipalRadio(BuildContext context) {
    return Image.asset(
      'assets/images/radio.png',
      width: MediaQuery.of(context).size.width * 0.6,
    );
  }
}

class _CustomBorder extends ShapeBorder {
  const _CustomBorder();

  @override
  EdgeInsetsGeometry get dimensions {
    return const EdgeInsets.only();
  }

  @override
  Path getInnerPath(Rect rect, {TextDirection textDirection}) {
    return getOuterPath(rect, textDirection: textDirection);
  }

  @override
  Path getOuterPath(Rect rect, {TextDirection textDirection}) {
    return Path()
      ..moveTo(rect.left + rect.width / 2.0, rect.top)
      ..lineTo(rect.right - rect.width / 6, rect.top + rect.height / 3)
      ..lineTo(rect.right, rect.top + rect.height / 2.0)
      ..lineTo(rect.right - rect.width / 6, rect.top + 2 * rect.height / 3)
      ..lineTo(rect.left + rect.width / 2.0, rect.bottom)
      ..lineTo(rect.left + rect.width / 6, rect.top + 2 * rect.height / 3)
      ..lineTo(rect.left, rect.top + rect.height / 2.0)
      ..lineTo(rect.left + rect.width / 6, rect.top + rect.height / 3)
      ..close();
  }

  @override
  void paint(Canvas canvas, Rect rect, {TextDirection textDirection}) {}

  // This border doesn't support scaling.
  @override
  ShapeBorder scale(double t) {
    return null;
  }
}
