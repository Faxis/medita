import 'dart:math';
import 'package:flutter/material.dart';

class Cronometro extends StatefulWidget {
  Cronometro({Key key}) : super(key: key);

  @override
  _CronometroState createState() => _CronometroState();
}

class _CronometroState extends State<Cronometro> with TickerProviderStateMixin {
  AnimationController controller;
  Color _backgroundDefault = Color.fromRGBO(120, 100, 250, 1);
  String pausa = "Pausa", _estadoPrincipal = "_estadoPrincipal";
  int par = 1;

  String get timerString {
    Duration duration = controller.duration * controller.value;
    return '${duration.inMinutes}:${(duration.inSeconds % 60).toString().padLeft(2, '0')}';
  }

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
      vsync: this,
      duration: Duration(seconds: 60),
    );
  }

  //es como el onDestroy de Android
  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var animatedBuilder = AnimatedBuilder(
        animation: controller,
        builder: (context, child) {
          return Stack(
            children: <Widget>[
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  color: Colors.black26,
                  height: controller.value * MediaQuery.of(context).size.height,
                ),
              ),
              Padding(
                padding: EdgeInsets.all(20.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Expanded(
                      child: Align(
                        alignment: FractionalOffset.topCenter,
                        child: AspectRatio(
                          aspectRatio: 1.0,
                          child: Stack(
                            children: <Widget>[
                              Positioned.fill(
                                child: CustomPaint(
                                    painter: CustomTimerPainter(
                                  animation: controller,
                                  backgroundColor: Colors.black,
                                  color: Colors.black12,
                                )),
                              ),
                              Align(
                                alignment: FractionalOffset.center,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      "¡Tiempo de relajarnos!",
                                      style: TextStyle(
                                          fontSize: 20.0,
                                          color: Colors.black38),
                                    ),
                                    Text(
                                      timerString,
                                      style: TextStyle(
                                          fontSize: 90.0,
                                          color: Colors.black45),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    AnimatedBuilder(
                        animation: controller,
                        builder: (context, child) {
                          return botonAccion();
                        }),
                  ],
                ),
              ),
            ],
          );
        });
    return Scaffold(
      backgroundColor: _backgroundDefault,
      body: animatedBuilder,
    );
  }

  void funcionComparar() {
    if (controller.value == 0.0 || controller.value == 1.0) {
      _estadoPrincipal = "Iniciar";
    } else {
      if (controller.isAnimating) {
        _estadoPrincipal = "Pausar";
      } else {
        _estadoPrincipal = "Continuar";
      }
    }
  }

  Widget botonAccion() {
    funcionComparar();

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[stateButton(), reCargar()],
    );
  }

  FloatingActionButton reCargar() {
    return FloatingActionButton(
        heroTag: "opcionBoton2", //Evita el crasheo para cambiar de evento
        elevation: 0,
        splashColor: Colors.red,
        backgroundColor: Colors.black45,
        onPressed: () {
          setState(() {
            controller.stop();
            controller.reset();
          });
        },
        child: Icon(Icons.replay));
  }

  FloatingActionButton stateButton() {
    return FloatingActionButton.extended(
        elevation: 0,
        backgroundColor: Colors.black45,
        splashColor: Colors.orange,
        heroTag: "opcionBoton1",
        onPressed: () {
          if (controller.isAnimating) {
            controller.stop();
            setState(() {
              _estadoPrincipal = "Pausar";
            });
          } else {
            controller.reverse(
                from: controller.value == 0.0 ? 1.0 : controller.value);
          }
        },
        icon: Icon(controller.isAnimating ? Icons.pause : Icons.play_arrow),
        label: Text(_estadoPrincipal));
  }
}

class CustomTimerPainter extends CustomPainter {
  CustomTimerPainter({
    this.animation,
    this.backgroundColor,
    this.color,
  }) : super(repaint: animation);

  final Animation<double> animation;
  final Color backgroundColor, color;

  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()
      ..color = Colors.black26
      ..strokeWidth = 7.0
      ..strokeCap = StrokeCap.butt
      ..isAntiAlias = true
      ..style = PaintingStyle.stroke;

    canvas.drawCircle(size.center(Offset.zero), size.width / 2.0, paint);
    paint.color = color;
    paint.isAntiAlias = true;

    double progress = (1.0 - animation.value) * 2 * pi;

    canvas.drawArc(Offset.zero & size, pi * 1.5, -progress, false, paint);

    var path = new Path();

    path.moveTo(0.0, size.height / 2);
    path.lineTo(size.width / 2, size.height / 2);
    Rect rect = new Offset(0.0, 0.0) & size;

    paint
      ..shader = new LinearGradient(
              colors: [Colors.yellow[500], Colors.red],
              begin: Alignment.topRight,
              end: Alignment.bottomCenter)
          .createShader(rect)
      ..isAntiAlias = true;

    canvas.drawPath(
        path,
        paint
          ..color = Colors.red
          ..isAntiAlias = true
          ..style = PaintingStyle.fill);

    canvas.drawArc(Offset.zero & size, pi * 1.5, -progress, false, paint);
  }

  @override
  bool shouldRepaint(CustomTimerPainter old) {
    return animation.value != old.animation.value ||
        color != old.color ||
        backgroundColor != old.backgroundColor;
  }
}
