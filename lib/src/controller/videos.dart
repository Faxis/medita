import 'package:flutter_youtube/flutter_youtube.dart';

const apiKay = 'AIzaSyA8clqRyAEPTNP4EtiLZYg-yY_HlX-mRBM';

class VideosPage {
  String code;

  VideosPage(String code) {
    playYoutubeVideoIdEdit(code);
  }

  void playYoutubeVideoIdEdit(String code) async {
    FlutterYoutube.onVideoEnded.listen((onData) {
      print(onData);
    });

    await FlutterYoutube.playYoutubeVideoById(
        apiKey: apiKay, videoId: code, fullScreen: true, autoPlay: true);
  }
}
