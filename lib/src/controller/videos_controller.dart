import 'package:flutter/material.dart';
import 'package:medita/src/controller/videos.dart';
import 'package:youtube_api_v3/youtube_api_v3.dart';

const apiKey = 'AIzaSyA8clqRyAEPTNP4EtiLZYg-yY_HlX-mRBM';

class VideosController extends StatefulWidget {
  final idList, title;
  const VideosController({this.idList, this.title});

  @override
  VideosControllerState createState() => VideosControllerState();
}

class VideosControllerState extends State<VideosController> {
  List<PlayListItem> videos = new List();
  PlayListItemListResponse paginaActual;

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    obtenerVideos();
    //obtenerImage();
  }

  setVideos(videos) {
    setState(() {
      this.videos = videos;
    });
  }

  obtenerVideos() async {
    YoutubeAPIv3 api = new YoutubeAPIv3(apiKey);
    PlayListItemListResponse playlist = await api.playListItems(
        //playlistId: 'PL2rlMIrfyUv8fPPJxZC9vSHRDaTrNJs1_',
        playlistId: this.widget.idList,
        maxResults: 5,
        part: Parts.snippet);

    var videos = playlist.items.map((video) {
      return video;
    }).toList();
    paginaActual = playlist;
    this.videos.addAll(videos);
    setVideos(this.videos);
  }

  nextPage() async {
    PlayListItemListResponse playlist = await paginaActual.nextPage();
    var videos = playlist.items.map((video) {
      return video;
    }).toList();
    paginaActual = playlist;

    this.videos.addAll(videos);
    setVideos(this.videos);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(this.widget.title),
        backgroundColor: Color.fromRGBO(100, 130, 222, 1.0),
        excludeHeaderSemantics: true,
      ),
      body: listaCargada(),
    );
  }

  Widget listaCargada() {
    return Center(
        child: ListView.builder(
      padding: EdgeInsets.only(top: 10),
      itemCount: videos.length + 1,
      itemBuilder: (BuildContext context, int index) {
        if (videos.length == index) {
          return ListTile(
            onTap: () {
              nextPage();
            },
            trailing: Icon(Icons.add),
            title: Text('Cargar Mas'),
          );
        }
        var video = videos[index];
        return ListTile(
            title: Text(video.snippet.title),
            trailing: Icon(Icons.arrow_forward_ios),
            leading: ClipRRect(
              borderRadius: BorderRadius.circular(6),
              child: Image(
                  fit: BoxFit.cover,
                  width: MediaQuery.of(context).size.width * 0.2,
                  image: NetworkImage(video.snippet.thumbnails.high.url)),
            ),
            onTap: () {
              VideosPage(video.snippet.resourceId.videoId);
            });
      },
    ));
  }
}

class Video {
  final String thumbnail;
  Video(this.thumbnail);
}
