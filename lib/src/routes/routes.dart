import 'package:flutter/material.dart';
import 'package:medita/src/pages/contacto.dart';
import 'package:medita/src/pages/musica.dart';

import 'package:medita/src/pages/principal.dart';
import 'package:medita/src/pages/tecnicas.dart';

Map<String, WidgetBuilder> getApplicationRoutes() {
  return <String, WidgetBuilder>{
    '/': (BuildContext context) => Principal(),
    'contacto': (BuildContext context) => Contacto(),
    'tecnicas': (BuildContext context) => Tecnicas(),
    'musica': (BuildContext context) => Musica(),
    
  };
}
